package application.main;

import com.jfoenix.controls.*;
import com.jfoenix.validation.DoubleValidator;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Text;

import java.io.File;
import java.net.URL;
import java.util.*;

public class Controller implements Initializable {

    @FXML
    private JFXTextField inputField;

    @FXML
    private Label error;
    @FXML
    private JFXComboBox<Label> from;

    @FXML
    private JFXComboBox<Label> to;
    @FXML
    private JFXButton convertBtn;

    @FXML
    private StackPane pane;

    private ObservableList<Label> list = FXCollections.observableArrayList();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        CurrencyConvert convert = new CurrencyConvert(this);
        Map<String, String> countries = new HashMap<>();
        for (String iso : Locale.getISOCountries()) {
            Locale l = new Locale("", iso);
            countries.put(l.getDisplayCountry(), iso);
        }
        list = getCountries();
        from.setItems(list);
        to.setItems(list);

        DoubleValidator doubleValidator = new DoubleValidator();
        doubleValidator.setMessage("Разрешены только числа");
        inputField.getValidators().addAll(doubleValidator);

        convertBtn.addEventHandler(javafx.scene.input.MouseEvent.MOUSE_CLICKED, e -> {
            error.setText("");
            if (validateInput()) {
                String fromCountry = from.getSelectionModel().getSelectedItem().getText();
                String toCountry = to.getSelectionModel().getSelectedItem().getText();
                String fromCurrency = Currency.getInstance(new Locale("", countries.get(fromCountry))).getCurrencyCode();
                String toCurrency = Currency.getInstance(new Locale("", countries.get(toCountry))).getCurrencyCode();
                Platform.runLater(() -> {
                    double rate = convert.convert(fromCurrency, toCurrency);
                    rate = rate * Double.parseDouble(inputField.getText());
                    showDialog("Результат \n",
                            "Конвертировано из " + from.getSelectionModel().getSelectedItem().getText() +
                            "\n" + "в " + to.getSelectionModel().getSelectedItem().getText() +
                            "\n" + "сумма " + rate, pane);
                });
            } else {
                error.setText("Заполните все поля");
            }
        });
    }

    public ObservableList<Label> getCountries() {
        String[] locales = Locale.getISOCountries();
        ObservableList<Label> countries = FXCollections.observableArrayList();
        for (String countryCode : locales) {
            Locale obj = new Locale("EN", countryCode);
            Label country = new Label(obj.getDisplayCountry());
            if (!obj.getCountry().toLowerCase().equals("an")) {

                Image icon = new Image(new File("src/main/java/application/resources/icons/flags/4x3/" + obj.getCountry().toLowerCase() + ".png").toURI().toString());
                country.setGraphic(new ImageView(icon));
                countries.add(country);
            }
        }
        return countries;
    }

    public void showDialog(String heading, String body, StackPane stackPane) {
        JFXButton cancel = new JFXButton("OK");
        cancel.setPrefSize(112, 35);
        JFXDialogLayout content = new JFXDialogLayout();
        content.setHeading(new Text(heading));
        content.setBody(new Text(body));
        content.setActions(cancel);
        JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER);
        dialog.show();
        cancel.setOnAction(event -> dialog.close());
    }

    private boolean validateInput() {
        if (inputField.validate()) {
            if (from.getSelectionModel().getSelectedIndex() != -1 && to.getSelectionModel().getSelectedIndex() != -1) {
                return true;
            }
        }
        return false;
    }

    public Label getError() {
        return error;
    }

    public void setError(Label error) {
        this.error = error;
    }
}
