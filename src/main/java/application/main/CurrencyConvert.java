package application.main;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;

public class CurrencyConvert {

    private static final String API_PROVIDER = "https://api.exchangeratesapi.io";
    private Controller controller;

    public CurrencyConvert(Controller controller) {
        this.controller = controller;
    }

    public double convert(String fromCurrencyCode, String toCurrencyCode) {
        System.out.println("fromCurrencyCode = " + fromCurrencyCode + " toCurrencyCode = " + toCurrencyCode);

        FixerResponse response = getResponse(API_PROVIDER + "/latest?base=" + fromCurrencyCode);
        if (response != null) {
            String rate = response.getRates().get(toCurrencyCode);
            double conversionRate;
            conversionRate = Double.parseDouble((rate != null) ? rate : "0.0");
            return conversionRate;
        }
        return 0.0;
    }

    private FixerResponse getResponse(String strUrl) {

        FixerResponse response = null;

        Gson gson = new Gson();
        StringBuffer sb = new StringBuffer();

        URL url;
        try {
            url = new URL(strUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            InputStream stream = connection.getInputStream();
            int data = stream.read();

            while (data != -1) {
                sb.append((char) data);
                data = stream.read();
            }
            stream.close();
            response = gson.fromJson(sb.toString(), FixerResponse.class);

        } catch (MalformedURLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } catch (UnknownHostException ex) {
            controller.getError().setText("Неизвестная ошибка");
        } catch (IOException e) {
            controller.getError().setText("Нет данных для конвертирования в эту валюту или из этой");
        }

        return response;
    }

}
